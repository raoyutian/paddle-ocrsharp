package main

import (
    "fmt"
    "syscall"
    "unsafe"
    "os"
    "bufio"
    "C"
    "io/ioutil"
    "time"
)

// 获取字符串的长度指针
func lenPtr(s string) uintptr {
    return uintptr(len(s))
}

// 获取数字的指针
func intPtr(n int) uintptr {
    return uintptr(n)
}

// 获取字符串的指针
func strPtr(s string) uintptr {
    return uintptr(unsafe.Pointer(syscall.StringBytePtr(s)))
}

func getCurrentAbPathBywd() string {
	dir, err := os.Getwd()
	if err!=nil {
        fmt.Println(err)
	}
	return dir
}

func main() {  
   
 dll,_:= syscall.LoadDLL("PaddleOCR.dll")
 Initjson,_:=dll.FindProc("Initializejson")
 detect,_:=dll.FindProc("Detect")
 enableANSI,_:=dll.FindProc("EnableANSIResult")
 enableJson,_:=dll.FindProc("EnableJsonResult")
root:=getCurrentAbPathBywd();
Initjson.Call(strPtr(root+"\\inference\\ch_PP-OCRv4_det_infer"),
strPtr(root+"\\inference\\ch_ppocr_mobile_v2.0_cls_infer"),
strPtr(root+"\\inference\\ch_PP-OCRv4_rec_infer"),
strPtr(root+"\\inference\\ppocr_keys.txt"),strPtr("{}"))

//启用单字节编码返回json串（默认为Unicode，有空格，go不能显示全）
enableANSI.Call(1)//0，unicode编码，1，ANSI编码
enableJson.Call(0)//0，返回纯字符串结果，1，返回json字符串结果

files, err := ioutil.ReadDir(root+"\\image")
    if err != nil {
         fmt.Println(err)
    }

for _, file := range files {
     fmt.Println(file.Name())
     start := time.Now() // 记录开始时间   
     res, _, _:=detect.Call(strPtr(root+"\\image\\"+file.Name()))  
     p_result := (*C.char)(unsafe.Pointer(res))
     end := time.Now()   // 记录结束时间
     elapsed := end.Sub(start) // 计算开始和结束时间的差值
     ms := float64(elapsed)/float64(time.Millisecond)
     fmt.Printf("耗时: %.2fms\n", ms) // 打印执行时间
     ocrresult:= C.GoString(p_result)
     fmt.Println(ocrresult)
    }

 input := bufio.NewScanner(os.Stdin)
 input.Scan() 
}